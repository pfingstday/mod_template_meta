%% Copyright
-module(m_template).
-author("Steffen Hanikel").

-behaviour(gen_model).

%% interface functions
-export(
[
  m_find_value/3,
  m_to_list/2,
  m_value/2
]).

-include_lib("zotonic.hrl").

-record(template,{name, id, state}).

%% @doc Fetch the value for the key from a model source
%% @spec m_find_value(Key, Source, Context) -> term()
m_find_value(by_id, #m{value=undefined} = M, _Context) ->
  M#m{value=by_id};
m_find_value(by_path, #m{value=undefined} = M, _Context) ->
  M#m{value=by_path};
m_find_value(by_name, #m{value=undefined} = M, _Context) ->
  M#m{value=by_name};
m_find_value(Id, #m{value=by_id} = M, Context) ->
  Url = binary_to_list(m_rsc:p_no_acl(Id, page_url ,Context)),
  M#m{value=#template{name=pathToTemplate(Url, Context),id=Id}};
m_find_value(Path, #m{value=by_path} = M, Context) ->
  M#m{value=#template{name=pathToTemplate(Path, Context),id=m_rsc:page_path_to_id(Path, Context)}};
m_find_value(Name, #m{value=by_name} = M, _Context) ->
  M#m{value=#template{name=Name}};
m_find_value(Keyword, #m{value=#template{} = V} = M, _Context) ->
  M#m{value=V#template{state=Keyword}};
m_find_value(_Id, #m{} = M, _Context) ->
  ?DEBUG(_Id),
  ?DEBUG(M),
  M.


%% @doc Transform a m_config value to a list, used for template loops
%% @spec m_to_list(Source, Context) -> list()
m_to_list(#m{value=#template{state=State, name=Name, id=Id}}, Context) ->
  proplists:get_value(State, getBlockList(Name, Id, Context), []);
m_to_list(#m{}, _Context) ->
  [].


%% @doc Transform a model value so that it can be formatted or piped through filters
%% @spec m_value(Source, Context) -> term()
m_value(#m{} = M, _Context) ->
  ?DEBUG(M),
  undefined.

getBlockList(Name, Id, Context) ->
  readMetaData(findMetaFileForTemplate(Name, Id, Context)).

pathToTemplate(Path, Context) ->
  {Host, Hostname, _Streamhost, _Smtphost, _Hostaliases, _Redirect, SiteDispatch} = z_dispatcher:dispatchinfo(Context),
  Rule = wm_dispatch(http, Hostname, Host, Path, SiteDispatch),
  dispatchRuleToTemplate(Rule).

dispatchRuleToTemplate({_,controller_template,L,_,_,_,_}) ->
  proplists:get_value(template,L);
dispatchRuleToTemplate({_,controller_page,L,_,_,_,_}) ->
  proplists:get_value(template,L).

findMetaFileForTemplate(Name, Id, Context) ->
  case find_template(Name, Id, Context) of
    {error, enoent} ->
      undefined;
    {ok, #module_index{} = Index} ->
      Index#module_index.filepath
  end.

find_template({cat, Name}, Id, Context) ->
  z_template:find_template_cat(Name, Id, Context);
find_template(Name, _Id, Context) ->
  z_template:find_template(Name, Context).



readMetaData(File) ->
  case file:consult([File, ".meta"]) of
    {error, _Reason} ->
      [];
    {ok, Terms} ->
      Terms
  end.








-define(SEPARATOR, $\/).
-define(MATCH_ALL, '*').

%% @spec wm_dispatch(Protocol, HostAsString, Host::atom(), Path::string(), DispatchList::[matchterm()]) ->
%%                                            dispterm() | dispfail()
%% @doc Interface for URL dispatching.
%% See also http://bitbucket.org/justin/webmachine/wiki/DispatchConfiguration
wm_dispatch(Protocol, HostAsString, Host, PathAsString, DispatchList) ->
  Context = z_context:new(Host),
  Path = string:tokens(PathAsString, [?SEPARATOR]),
  IsDir = lists:last(PathAsString) == ?SEPARATOR,
  {Path1, Bindings} = z_notifier:foldl(#dispatch_rewrite{is_dir=IsDir, path=PathAsString, host=HostAsString}, {Path, []}, Context),
  try_path_binding(Protocol, HostAsString, Host, DispatchList, Path1, Bindings, extra_depth(Path1, IsDir), Context).

% URIs that end with a trailing slash are implicitly one token
% "deeper" than we otherwise might think as we are "inside"
% a directory named by the last token.
extra_depth([], _IsDir) -> 1;
extra_depth(_Path, true) -> 1;
extra_depth(_, _) -> 0.


try_path_binding(_Protocol, _HostAsString, _Host, [], PathTokens, Bindings, _ExtraDepth, _Context) ->
  {no_dispatch_match, PathTokens, Bindings};
try_path_binding(Protocol, HostAsString, Host, [{DispatchName, PathSchema, Mod, Props}|Rest], PathTokens, Bindings, ExtraDepth, Context) ->
  case bind(Host, PathSchema, PathTokens, Bindings, 0, Context) of
    {ok, Remainder, NewBindings, Depth} ->
      case proplists:get_value(protocol, Props) of
      % Force switch to normal http protocol
        undefined when Protocol =/= http ->
          {Host1, HostPort} = split_host(z_context:hostname_port(Context)),
          Host2 = add_port(http, Host1, HostPort),
          {redirect_protocol, "http", Host2};

      % Force switch to other (eg. https) protocol
        {NewProtocol, NewPort} when NewProtocol =/= Protocol ->
          {Host1, _Port} = split_host(HostAsString),
          Host2 = add_port(NewProtocol, Host1, NewPort),
          {redirect_protocol, z_convert:to_list(NewProtocol), Host2};

      % 'keep' or correct protocol
        _ ->
          {DispatchName, Mod, Props, Remainder, NewBindings,
            calculate_app_root(Depth + ExtraDepth),
            reconstitute(Remainder)}
      end;
    fail ->
      try_path_binding(Protocol, HostAsString, Host, Rest, PathTokens, Bindings, ExtraDepth, Context)
  end.

add_port(http, Host, 80) -> Host;
add_port(https, Host, 443) -> Host;
add_port(_, Host, Port) -> Host++[$:|z_convert:to_list(Port)].

bind(_Host, [], [], Bindings, Depth, _Context) ->
  {ok, [], Bindings, Depth};
bind(_Host, [?MATCH_ALL], Rest, Bindings, Depth, _Context) when is_list(Rest) ->
  {ok, Rest, Bindings, Depth + length(Rest)};
bind(_Host, _, [], _, _, _Context) ->
  fail;
bind(Host, [z_language|RestToken],[Match|RestMatch],Bindings,Depth, Context) ->
  case z_trans:is_language(Match) of
    true -> bind(Host, RestToken, RestMatch, [{z_language, Match}|Bindings], Depth + 1, Context);
    false -> fail
  end;
bind(Host, [Token|RestToken],[Match|RestMatch],Bindings,Depth, Context) when is_atom(Token) ->
  bind(Host, RestToken, RestMatch, [{Token, Match}|Bindings], Depth + 1, Context);
bind(Host, [{Token, {Module,Function}}|RestToken],[Match|RestMatch],Bindings,Depth, Context)
  when is_atom(Token), is_atom(Module), is_atom(Function) ->
  case Module:Function(Match, Context) of
    true -> bind(Host, RestToken, RestMatch, [{Token, Match}|Bindings], Depth + 1, Context);
    false -> fail;
    {ok, Value} -> bind(Host, RestToken, RestMatch, [{Token, Value}|Bindings], Depth+1, Context)
  end;
bind(Host, [{Token, RegExp}|RestToken],[Match|RestMatch],Bindings,Depth, Context) when is_atom(Token) ->
  case re:run(Match, RegExp) of
    {match, _} -> bind(Host, RestToken, RestMatch, [{Token, Match}|Bindings], Depth+1, Context);
    nomatch -> fail
  end;
bind(Host, [{Token, RegExp, Options}|RestToken],[Match|RestMatch],Bindings,Depth, Context) when is_atom(Token) ->
  case re:run(Match, RegExp, Options) of
    {match, []} -> bind(Host, RestToken, RestMatch, [{Token, Match}|Bindings], Depth+1, Context);
    {match, [T|_]} when is_tuple(T) -> bind(Host, RestToken, RestMatch, [{Token, Match}|Bindings], Depth+1, Context);
    {match, [Captured]} -> bind(Host, RestToken, RestMatch, [{Token, Captured}|Bindings], Depth+1, Context);
    {match, Captured} -> bind(Host, RestToken, RestMatch, [{Token, Captured}|Bindings], Depth+1, Context);
    match -> bind(Host, RestToken, RestMatch, [{Token, Match}|Bindings], Depth+1, Context);
    nomatch -> fail
  end;
bind(Host, [Token|RestToken], [Token|RestMatch], Bindings, Depth, Context) ->
  bind(Host, RestToken, RestMatch, Bindings, Depth + 1, Context);
bind(_Host, _, _, _, _, _Context) ->
  fail.

reconstitute([]) -> "";
reconstitute(UnmatchedTokens) -> string:join(UnmatchedTokens, [?SEPARATOR]).

calculate_app_root(1) -> ".";
calculate_app_root(N) when N > 1 ->
  string:join(lists:duplicate(N, ".."), [?SEPARATOR]).

split_host(Host) ->
  case Host of
    undefined -> {"", "80"};
    none -> {"", "80"};
    [] -> {"", "80"};
    _ ->
      % Split the optional port number from the host name
      [H|Rest] = string:tokens(string:to_lower(Host), ":"),
      case Rest of
        [] -> {H, "80"};
        [Port|_] -> {H, Port}
      end
  end.