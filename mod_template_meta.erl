-module(mod_template_meta).
-author("Steffen Hanikel <steffen.hanikel@gmail.com>").

-mod_title("Template Meta").
-mod_description("Provides Metainformation for templates.").
-mod_prio(900).
-mod_depends([base]).
-mod_provides([template_meta]).
